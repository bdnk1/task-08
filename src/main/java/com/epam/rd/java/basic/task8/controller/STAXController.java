package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Main;
import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.FlowerParameters;
import com.epam.rd.java.basic.task8.entity.GrowingTips;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.stream.StreamSource;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler implements Parser {

    private String xmlFileName;

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    @Override
    public List<Flower> parseFile() {
        XMLInputFactory factory = XMLInputFactory.newInstance();
        factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);
        XMLEventReader reader = null;
        List<Flower> flowers = null;
        try {
            reader = factory.createXMLEventReader(new StreamSource(Main.class.getClassLoader().getResourceAsStream(xmlFileName)));
            flowers = new ArrayList<>();
            Flower flower = null;
            FlowerParameters params = null;
            GrowingTips tips = null;
            while (reader.hasNext()) {
                XMLEvent event = reader.nextEvent();
                if (event.isStartElement()) {
                    StartElement startElement = event.asStartElement();
                    switch (startElement.getName().getLocalPart()) {
                        case "flower":
                            flower = new Flower();
                            break;
                        case "name":
                            event = reader.nextEvent();
                            flower.setName(event.asCharacters().getData());
                            break;
                        case "soil":
                            event = reader.nextEvent();
                            flower.setSoil(event.asCharacters().getData());
                            break;
                        case "origin":
                            event = reader.nextEvent();
                            flower.setOrigin(event.asCharacters().getData());
                            break;
                        case "visualParameters":
                            params = new FlowerParameters();
                            break;
                        case "stemColour":

                            event = reader.nextEvent();
                            params.setStemColour(event.asCharacters().getData());
                            break;
                        case "leafColour":
                            event = reader.nextEvent();
                            params.setLeafColour(event.asCharacters().getData());
                            break;
                        case "aveLenFlower":
                             params.setLenMeasurment(startElement.getAttributeByName(new QName("measure")).getValue());
                            event = reader.nextEvent();
                            params.setAveLenFlower(Integer.parseInt(event.asCharacters().getData()));
                            break;
                        case "growingTips":
                            tips = new GrowingTips();
                            break;
                        case "tempreture":
                            tips.setTemperatureMeasuring(startElement.getAttributeByName(new QName("measure")).getValue());
                            event = reader.nextEvent();
                            tips.setTemperature(Integer.parseInt(event.asCharacters().getData()));
                            break;
                        case "lighting":
                            tips.setLighting(startElement.getAttributeByName(new QName("lightRequiring")).getValue().equals("yes"));
                            break;
                        case "watering":
                            tips.setWateringMeasure(startElement.getAttributeByName(new QName("measure")).getValue());

                            event = reader.nextEvent();
                            tips.setWatering(Integer.parseInt(event.asCharacters().getData()));
                            break;
                        case "multiplying":
                            event = reader.nextEvent();
                            flower.setMultypling(event.asCharacters().getData());
                            break;
                    }
                }
                if (event.isEndElement()) {
                    EndElement endElement = event.asEndElement();
                    if (endElement.getName().getLocalPart().equals("flower")) {
                        flowers.add(flower);
                    } else if (endElement.getName().getLocalPart().equals("visualParameters")) {
                        flower.setVisualParameters(params);
                    } else if (endElement.getName().getLocalPart().equals("growingTips")) {
                        flower.setTips(tips);
                    }
                }
            }

        } catch (XMLStreamException e) {
            throw new RuntimeException(e);
        }
        return flowers;
    }

    /*public Flower parseEvent(XMLEvent event) throws XMLStreamException {

    }*/
}

