package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Main;
import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.FlowerParameters;
import com.epam.rd.java.basic.task8.entity.GrowingTips;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler implements Parser {

    private String xmlFileName;

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    @Override
    public List<Flower> parseFile() {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setNamespaceAware(true);
        try {
            SAXParser parser = factory.newSAXParser();
            CustomSaxHandler customHandler = new CustomSaxHandler();
            parser.parse(Main.class.getClassLoader().getResourceAsStream(xmlFileName), customHandler);
            return customHandler.getFlowersList();
        } catch (SAXException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        }

    }
}

class CustomSaxHandler extends DefaultHandler {
    private List<Flower> flowers;
    private StringBuilder elementValue;

    @Override
    public void startDocument() throws SAXException {
        flowers = new ArrayList<>();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        switch (qName) {
            case "flower":
                flowers.add(new Flower());
                break;
            case "name":
                elementValue = new StringBuilder();
                break;
            case "soil":
                elementValue = new StringBuilder();
                break;
            case "origin":
                elementValue = new StringBuilder();
                break;
            case "visualParameters":
                latestFlower().setVisualParameters(new FlowerParameters());
                break;
            case "growingTips":
                latestFlower().setTips(new GrowingTips());
                break;
            case "stemColour":
                elementValue = new StringBuilder();
                break;
            case "leafColour":
                elementValue = new StringBuilder();
                break;
            case "aveLenFlower":
                latestFlowerParams().setLenMeasurment(attributes.getValue(0));;
                elementValue = new StringBuilder();
                break;
            case "tempreture":
                latestTips().setTemperatureMeasuring(attributes.getValue(0));
                elementValue = new StringBuilder();
                break;
            case "lighting":
                elementValue = new StringBuilder();
                break;
            case "watering":
                latestTips().setWateringMeasure(attributes.getValue(0));
                elementValue = new StringBuilder();
                break;
            case "multiplying":
                elementValue = new StringBuilder();
                break;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (elementValue == null) {
            elementValue = new StringBuilder();
        } else {
            elementValue.append(ch, start, length);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        switch (qName) {
            case "name":
                latestFlower().setName(elementValue.toString());
                break;
            case "soil":
                latestFlower().setSoil(elementValue.toString());
                break;
            case "multiplying":
                latestFlower().setMultypling(elementValue.toString());
                break;
            case "origin":
                latestFlower().setOrigin(elementValue.toString());
                break;
            case "stemColour":
                latestFlowerParams().setStemColour(elementValue.toString());
                break;
            case "leafColour":
                latestFlowerParams().setLeafColour(elementValue.toString());
                break;
            case "aveLenFlower":
                latestFlowerParams().setAveLenFlower(Integer.parseInt(elementValue.toString()));
                break;
            case "tempreture":
                latestTips().setTemperature(Integer.parseInt(elementValue.toString()));
                break;
            case "lighting":
                latestTips().setLighting(elementValue.toString().equals("yes"));
                break;
            case "watering":
                latestTips().setWatering(Integer.parseInt(elementValue.toString()));
                break;
        }
    }


    public Flower latestFlower() {
        return flowers.get(flowers.size() - 1);
    }

    public FlowerParameters latestFlowerParams() {
        return latestFlower().getVisualParameters();
    }

    public GrowingTips latestTips() {
        return latestFlower().getTips();
    }

    public List<Flower> getFlowersList() {
        return flowers;
    }
}
