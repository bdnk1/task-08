package com.epam.rd.java.basic.task8.entity;

public class GrowingTips {
    int temperature;
    boolean lighting;
    int watering;
    String wateringMeasure;
    String temperatureMeasuring;

    public GrowingTips(int temperature, boolean lighting, int watering, String wateringMeasure, String temperatureMeasuring) {
        this.temperature = temperature;
        this.lighting = lighting;
        this.watering = watering;
        this.wateringMeasure = wateringMeasure;
        this.temperatureMeasuring = temperatureMeasuring;
    }

    public GrowingTips(){}

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public void setWateringMeasure(String wateringMeasure) {
        this.wateringMeasure = wateringMeasure;
    }

    public void setTemperatureMeasuring(String temperatureMeasuring) {
        this.temperatureMeasuring = temperatureMeasuring;
    }

    public String getWateringMeasure() {
        return wateringMeasure;
    }

    public String getTemperatureMeasuring() {
        return temperatureMeasuring;
    }

    public void setLighting(boolean lighting) {
        this.lighting = lighting;
    }

    public void setWatering(int watering) {
        this.watering = watering;
    }

    @Override
    public String toString() {
        return "GrowingTips{" +
                "temperature=" + temperature +
                ", lighting=" + lighting +
                ", watering=" + watering +
                ", wateringMeasure='" + wateringMeasure + '\'' +
                ", temperatureMeasuring='" + temperatureMeasuring + '\'' +
                '}';
    }

    public int getTemperature() {
        return temperature;
    }

    public boolean isLighting() {
        return lighting;
    }

    public int getWatering() {
        return watering;
    }
}
