package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Flower;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            return;
        }

        String xmlFileName = args[0];
        System.out.println("Input ==> " + xmlFileName);

        // DOM
        DOMController domController = new DOMController(xmlFileName);
        List<Flower> flowersDom = domController.parseFile();
        flowersDom = flowersDom.stream().sorted(Comparator.comparing(Flower::getName)).collect(Collectors.toList());
        String outputXmlFile = "output.dom.xml";
        System.out.println(flowersDom);
        generateXml(flowersDom, outputXmlFile);


        //SAX
        SAXController saxController = new SAXController(xmlFileName);
        List<Flower> flowersSax = saxController.parseFile();
        System.out.println(flowersSax);
        flowersSax = flowersSax.stream().sorted(Comparator.comparing(Flower::getSoil)).collect(Collectors.toList());
        outputXmlFile = "output.sax.xml";
        generateXml(flowersSax, outputXmlFile);

        // StAX

        STAXController staxController = new STAXController(xmlFileName);
        List<Flower> flowersStax = staxController.parseFile();
        flowersStax = flowersStax.stream().sorted(Comparator.comparing(Flower::getSoil)).collect(Collectors.toList());
        outputXmlFile = "output.stax.xml";
        generateXml(flowersStax, outputXmlFile);

       }


    public static void generateXml(List<Flower> flowers, String outputFileName) throws ParserConfigurationException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("flowers");
        rootElement.setAttribute("xmlns","http://www.nure.ua");
        rootElement.setAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
        rootElement.setAttribute("xsi:schemaLocation","http://www.nure.ua input.xsd");
        doc.appendChild(rootElement);

        for(Flower flower: flowers){
            Element flowerElement = createFlowerElement(doc, flower);
            rootElement.appendChild(flowerElement);
        }

        try (FileOutputStream output = new FileOutputStream(outputFileName)) {
            writeXml(doc, output);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Element createFlowerElement(Document doc, Flower flower) {
        Element flowerElement = doc.createElement("flower");
        Element flowerName = doc.createElement("name");
        flowerName.setTextContent(flower.getName());
        Element flowerSoil = doc.createElement("soil");
        flowerSoil.setTextContent(flower.getSoil());
        Element flowerOrigin = doc.createElement("origin");
        flowerOrigin.setTextContent(flower.getOrigin());
        Element flowerParams = createFlowerParamsElement(doc,flower);
        Element growingTips = createGrowingTipsElement(doc,flower);
        Element multiplying = doc.createElement("multiplying");
        multiplying.setTextContent(flower.getMultypling());
        flowerElement.appendChild(flowerName);
        flowerElement.appendChild(flowerSoil);
        flowerElement.appendChild(flowerOrigin);
        flowerElement.appendChild(flowerParams);
        flowerElement.appendChild(growingTips);
        flowerElement.appendChild(multiplying);
        return flowerElement;
    }

    private static Element createFlowerParamsElement(Document doc, Flower flower){
        Element flowerParams = doc.createElement("visualParameters");

        Element stemColour = doc.createElement("stemColour");
        stemColour.setTextContent(flower.getVisualParameters().getStemColour());
        flowerParams.appendChild(stemColour);
        Element leafColour = doc.createElement("leafColour");
        leafColour.setTextContent(flower.getVisualParameters().getLeafColour());
        Element aveLenFlower = doc.createElement("aveLenFlower");
        aveLenFlower.setAttribute("measure",flower.getVisualParameters().getLenMeasurment());
        aveLenFlower.setTextContent(String.valueOf(flower.getVisualParameters().getAveLenFlower()));

        flowerParams.appendChild(stemColour);
        flowerParams.appendChild(leafColour);
        flowerParams.appendChild(aveLenFlower);
        return flowerParams;
    }

    private static Element createGrowingTipsElement(Document doc, Flower flower) {
        Element flowerParams = doc.createElement("growingTips");

        Element tempreture = doc.createElement("tempreture");
        tempreture.setAttribute("measure",flower.getTips().getTemperatureMeasuring());
        tempreture.setTextContent(String.valueOf(flower.getTips().getTemperature()));
        Element lighting = doc.createElement("lighting");
        lighting.setAttribute("lightRequiring",flower.getTips().isLighting()?"no":"yes");
        Element watering = doc.createElement("watering");
        watering.setAttribute("measure",flower.getTips().getWateringMeasure());
        watering.setTextContent(String.valueOf(flower.getTips().getWatering()));

        flowerParams.appendChild(tempreture);
        flowerParams.appendChild(lighting);
        flowerParams.appendChild(watering);
        return flowerParams;
    }

    private static void writeXml(Document doc, OutputStream output) {

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        try {
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(output);
            transformer.transform(source, result);
        } catch (TransformerException e) {
            throw new RuntimeException(e);
        }


    }

}
