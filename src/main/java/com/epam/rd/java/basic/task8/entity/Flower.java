package com.epam.rd.java.basic.task8.entity;

public class Flower {
    String name;
    String soil;
    String origin;
    FlowerParameters visualParameters;
    GrowingTips tips;
    String multypling;

    public Flower(String name, String soil, String origin, FlowerParameters visualParameters, GrowingTips tips, String multypling) {
        this.name = name;
        this.soil = soil;
        this.origin = origin;
        this.visualParameters = visualParameters;
        this.tips = tips;
        this.multypling = multypling;
    }

    public Flower() {}

    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", soil='" + soil + '\'' +
                ", origin='" + origin + '\'' +
                ", visualParameters=" + visualParameters +
                ", tips=" + tips +
                ", multypling='" + multypling + '\'' +
                '}';
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setVisualParameters(FlowerParameters visualParameters) {
        this.visualParameters = visualParameters;
    }

    public void setTips(GrowingTips tips) {
        this.tips = tips;
    }

    public void setMultypling(String multypling) {
        this.multypling = multypling;
    }

    public String getName() {
        return name;
    }

    public String getSoil() {
        return soil;
    }

    public String getOrigin() {
        return origin;
    }

    public FlowerParameters getVisualParameters() {
        return visualParameters;
    }

    public GrowingTips getTips() {
        return tips;
    }

    public String getMultypling() {
        return multypling;
    }
}
