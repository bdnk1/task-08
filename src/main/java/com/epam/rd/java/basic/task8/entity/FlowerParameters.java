package com.epam.rd.java.basic.task8.entity;

public class FlowerParameters {
    String stemColour;
    String leafColour;
    int aveLenFlower;
    String lenMeasurment;

    public FlowerParameters(String stemColour, String leafColour, int aveLenFlower,String lenMeasurment) {
        this.stemColour = stemColour;
        this.leafColour = leafColour;
        this.aveLenFlower = aveLenFlower;
        this.lenMeasurment = lenMeasurment;
    }

    @Override
    public String toString() {
        return "FlowerParameters{" +
                "stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlower=" + aveLenFlower +
                ", lenMeasurment='" + lenMeasurment + '\'' +
                '}';
    }

    public String getLenMeasurment() {
        return lenMeasurment;
    }

    public void setLenMeasurment(String lenMeasurment) {
        this.lenMeasurment = lenMeasurment;
    }

    public FlowerParameters() {
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public void setAveLenFlower(int aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    public String getStemColour() {
        return stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public int getAveLenFlower() {
        return aveLenFlower;
    }
}
