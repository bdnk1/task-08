package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;

import java.util.List;

public interface Parser {
    List<Flower> parseFile();
}
